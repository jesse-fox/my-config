
# MariaDB / MySQL

CREATE DATABASE example;

CREATE USER 'example'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON example.* TO 'example'@'localhost';

FLUSH PRIVILEGES;






# Postgres

sudo -u postgres psql
postgres=# create database mydb;
postgres=# create user myuser with encrypted password 'mypass';
postgres=# grant all privileges on database mydb to myuser;


\c mydb

GRANT ALL ON table TO my_user;

GRANT CONNECT ON DATABASE my_db TO my_user;
GRANT USAGE ON SCHEMA public TO my_user;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO my_user;