#!/bin/bash
# Made for Linux Mint 19.3

git config core.fileMode false

sudo apt update && sudo apt upgrade -y


# Dev packages
sudo apt install -y git nginx mariadb-server  
sudo apt install -y ruby ruby-dev ruby-bundler 
sudo apt install -y php php-gd php-fpm php-dom php-curl php-mysql

# Basic Pack
sudo apt install -y fail2ban chromium-browser blender krita gparted

# Game Pack
sudo apt install -y steam dolphin desmume

# Misc Pack
sudo apt install -y apt-transport-https




# Install apps that aren't in apt
cd ~/Downloads

# Gitkraken
wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
sudo apt install -y ./gitkraken-amd64.deb

# VSCodium
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add - 
echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list 
sudo apt update && sudo apt install -y codium 

# SublimeText
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt update && sudo apt install -y sublime-text

# Keeweb
wget https://github.com/keeweb/keeweb/releases/download/v1.12.3/KeeWeb-1.12.3.linux.x64.deb
sudo apt install -y ./KeeWeb-1.12.3.linux.x64.deb

# Discord
wget https://dl.discordapp.net/apps/linux/0.0.9/discord-0.0.9.deb
sudo apt install -y ./discord-0.0.9.deb

# qbtorrent
sudo add-apt-repository -y ppa:qbittorrent-team/qbittorrent-stable
sudo apt update && sudo apt install -y qbittorrent 

# Cryptomator
sudo add-apt-repository -y ppa:sebastian-stenzel/cryptomator
sudo apt update && sudo apt install -y cryptomator 

# Seafile
sudo add-apt-repository -y ppa:seafile/seafile-client
sudo apt update && sudo apt install -y seafile-gui 

# Lutris
sudo add-apt-repository -y ppa:lutris-team/lutris
sudo apt update && sudo apt install -y lutris

# Mupen64py
wget https://iweb.dl.sourceforge.net/project/m64py/m64py-0.2.4/m64py_0.2.4-0_all.deb
sudo apt install -y ./m64py_0.2.4-0_all.deb

# Other setup


# Browser - load bookmarks, install ublock origin
# qbtorrent - set up VPN and secure settings
# Seafile - connect to cloud
# Cryptomator - set up to decrypt on start



# Alternate localhost for development
sudo bash -c 'echo "127.0.0.1   dev.local" >> /etc/hosts'

# increase inotify limit, used for gitkraken and others
echo fs.inotify.max_user_watches=99999 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p


# possibly needed to make steam work if it doesn't already do that
#sudo apt-get install libnvidia-gl-390:i386
#where libnvidia-gl-xxx is your number

#Install wine, vulkan, extra driver stuff
sudo dpkg --add-architecture i386
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key

sudo apt update
sudo apt install nvidia-driver-430 libnvidia-gl-430 libnvidia-gl-430:i386
sudo apt install libvulkan1 libvulkan1:i386



# For epsxe
sudo apt-get install libcurl3 -y
sudo apt-get install libsdl-ttf2.0 -y