
# Checklist of things to go through when wiping operating system.

For security's sake I like to reinstall my OS once or twice every year. It also helps to clean up unused stuff. 



 - Browser bookmarks

 - Other local browser data, such as chat logs

 - Databases

 - commit and push all outstanding code

 - Nginx configuration files

 - cryptocurrency

 - Game save files

 - Files throughout home directory.

